package Main;

import java.util.ArrayList;

public class CompanyComposite extends CompanyComponent {
	public CompanyComposite(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	ArrayList<CompanyComponent> companyArray = new ArrayList<>();

	@Override
	public void accept(Visitor visitor) {
		// TODO Auto-generated method stub
		System.out.println("Composite accept");

		visitor.visit(this);
	}

	public void addChild(CompanyComponent companyComponent) {
		companyArray.add(companyComponent);
	}

	public ArrayList<CompanyComponent> getChild() {
		return companyArray;
	}

}
