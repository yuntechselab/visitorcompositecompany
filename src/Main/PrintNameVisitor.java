package Main;

public class PrintNameVisitor extends Visitor{

	@Override
	public void visit(CompanyComponent companyComponent) {
		// TODO Auto-generated method stub
		System.out.println("PrintNameVisitor visit companyComponent");

		System.out.println(companyComponent.getName());
	}

	@Override
	public void visit(CompanyComposite companyComposite) {
		// TODO Auto-generated method stub
		System.out.println("PrintNameVisitor visit companyComposite");

		System.out.println(companyComposite.getName());

	}

	@Override
	public void visit(CompanyLeaf companyLeaf) {
		// TODO Auto-generated method stub
		System.out.println("PrintNameVisitor visit companyLeaf");

		System.out.println(companyLeaf.getName());

	}
	
}
