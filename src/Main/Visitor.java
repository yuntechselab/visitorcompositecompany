package Main;

public abstract class Visitor {
	public abstract void visit(CompanyComponent companyComponent);
	public abstract void visit(CompanyComposite companyComposite);
	public abstract void visit(CompanyLeaf companyLeaf);

}
