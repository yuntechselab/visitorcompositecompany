package Main;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PrintNameVisitor printNameVisitor = new PrintNameVisitor();
		CompanyComponent aaa = new CompanyComposite("aaa");
		CompanyComponent a1 = new CompanyComposite("a1");
		CompanyComponent a2 = new CompanyLeaf("a2");
		CompanyComponent a3 = new CompanyLeaf("a3");

		aaa.addChild(a1);
		aaa.addChild(a2);
		aaa.addChild(a3);

		for (CompanyComponent companyComponent : aaa.getChild()) {
			companyComponent.accept(printNameVisitor);
		}

		// aaa.accept(printNameVisitor);

	}

}
