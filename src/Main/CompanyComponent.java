package Main;

import java.util.ArrayList;

public abstract class CompanyComponent {
	private String name;
	
	
	
	
	public CompanyComponent(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public abstract void accept(Visitor visitor);

	public void addChild(CompanyComponent companyComoponent) {
	};

	public void removeChild(CompanyComponent companyComponent) {
	};

	public ArrayList<CompanyComponent> getChild() {
		return null;
	}
}
